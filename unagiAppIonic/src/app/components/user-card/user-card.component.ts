import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent implements OnInit {
  @Input() firstname: string = '';
  @Input() lastname: string = '';
  @Input() content: string = '';
  @Input() avatarUrl: string = '';
  @Input() userId: number;

  constructor(private router: Router) {}

  ngOnInit() {}

  userDetail() {
    if (!this.userId) {
      return;
    }
    this.router.navigateByUrl(`/user/${this.userId}`);
  }
}
