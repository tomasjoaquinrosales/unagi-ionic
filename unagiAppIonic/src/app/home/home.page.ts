import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { User, UsersResponse } from '../interfaces/user.interface';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  users: User[] = [];
  loading: any;

  constructor(
    private usersService: UsersService,
    private loadingController: LoadingController
  ) {}

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    this.getUsers();
  }

  async getUsers() {
    await this.presentLoading();
    this.usersService.getUsers().subscribe((response: UsersResponse) => {
      this.dismissLoading();
      this.users = response.data;
    });
  }

  async presentLoading() {
    await this.loading.present();
  }

  async dismissLoading() {
    await this.loading.dismiss();
  }
}
