import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';
import { UserResponse, Data } from '../interfaces/user.interface';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {
  userId: number;
  userData: Data;
  userSupport: any;
  loading: any;

  constructor(
    private usersService: UsersService,
    private routeActive: ActivatedRoute,
    private loadingController: LoadingController
  ) {
    this.userId = this.routeActive.snapshot.params.id;
  }

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    this.getUser();
  }

  async getUser() {
    await this.presentLoading();
    this.usersService
      .getUserById(this.userId)
      .subscribe((response: UserResponse) => {
        this.dismissLoading();
        this.userData = response.data;
        this.userSupport = response.support;
      });
  }

  async presentLoading() {
    await this.loading.present();
  }

  async dismissLoading() {
    await this.loading.dismiss();
  }
}
