# unagi-ionic

## Description

Inicio del proyecto con las siguientes características:

1. blank template de ionic
1. angular
1. ruteo
1. capacitor

## Deploy to Android Devices

- [ ] Ejecutar `ionic build --prod`. Compila el proyecto los web assets y prepara el deployment. www
- [ ] Ejecutar `ionic capacitor add android`. Agrega una nueva carpeta como lo hacia Cordova para los dispositivos android o iphone. capacitor.config.json
- [ ] Ejecutar `ionic capacitor run ios --livereload-url=http://localhost:8100`. Test local y livereload para que se refresque cuando encuentra un cambio. Abre el andriod studio, instala dependencias, el gradle, el dispositivo.

Reusltado:

| User List | User Detail |
| --------------- | --------------- |
| ![](unagi1.PNG) | ![](unagi2.PNG) |


## Deploy PWA

- [ ] Ejecutar `ng add @angular/pwa`. Agrega dependencias de angular
- [ ] Ejecutar `ionic build --prod`. Genera el archivo manifest.json o gsw-config.json
- [ ] Deploy a firebase por ejemplo

## Extra Questions


1. _**¿Cuáles son algunas de las funciones de Angular que más te gustan para Ionic y por qué las usás?**_

**Respuesta:** Las funciones que más me gusta de Angular son los Pipes que me parecen de más últiles para la transoformación de contenido y filtro y el uso de directivas es intuitivo y de fácil implementación y de forma sencilla se le agrega funcionalidad extra a un componente.

1. _**¿Usaste Typescript para proyectos Angular? Si lo hiciste, ¿qué es lo que más te gusta respecto a JavaScript?**_

**Respuesta:** Angular está pensado para usar Typescript según entiendo y se le pude agregar javascript, por ejemplo, si usaramos la libreria moment.js mediante una importación simple se complementa perfectamente. Tengo conocimientos más fuertes con javascript, pero la adaptación a Typescript es muy amigable, inclusive la docuementación que ellos prestan tienen en cuenta como pasar de un lenguaje a otro y donde prestar atención. Lo que más me gusta de javascript es poder desarrollar con libertad y mantener un orden al mismo tiempo.

1. _**¿Usaste decorators para refactorizar tu código? En caso de que lo hayas hecho, ¿qué resolviste?**_

**Respuesta:** Si en python, pero en especial en Flask y Django tienen como cabecera dos patrones muy conocidos el Factory y el Decorator. Actualmente utilicé decoradores para generar el loggeo de microservicios decorando cada vista con este patrón para hacer el segumiento de errores y aciertos en AWS CW.

1. _**¿Recordás algún plugin de Cordova que hayas usado y para qué lo usaste?**_

**Respuesta:** He utilizado algunos, pero el que más me acuerdo es el plugin de la cámara que lo utilicé para subir fotos de prodcutos y luego para hacer un scanner de código de barra, el de One Signal para manejar alertas asíncronas y el data storage para almacenamiento.

1. _**¿Usaste SCSS o algún pre-procesador de CSS?**___

**Respuesta:** SCSS toco muy de oido, me acostumbre a usar CSS puro.

1. _**¿Usaste bases de datos o algún tipo de almacenamiento con Ionic?**_

**Respuesta:** No del lado del cliente, a lo sumo el local storage, pero del lado del servidor use postresql y mongodb.

