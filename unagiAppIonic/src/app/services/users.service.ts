import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserResponse, UsersResponse } from '../interfaces/user.interface';
import { environment } from '../../environments/environment';

const URL_USERS = environment.urlApi;

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  /**
   * Returns the response that has the list of users
   */
  getUsers() {
    return this.http.get<UsersResponse>(`${URL_USERS}/users`);
  }

  /**
   * Returns the user object by id
   * @param userId number, user id
   */
  getUserById(userId: number) {
    return this.http.get<UserResponse>(`${URL_USERS}/users/${userId}`);
  }
}
